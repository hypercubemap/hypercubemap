HyperCubeMap
===============================

HyperCubeMap is a hyperbolic embedding scheme and optimization framework for social network ad allocation problem. It has two pieces:

* Its embedding algorithm arranges an arbitrary size complex network in hyperbolic space, and decompose it into optimization friendly unit impression graphs. 

* Given the bidding data, the optimization programs approximate allocation using hyperbolic regions, which results in a series of linear programs. 

Dependencies
----------------------------
* CPLEX 12.6 (earlier version may work)

Team 
----------------------------
* Hui Miao (hui@cs.umd.edu)
* Peixin Gao (gaopei@umd.edu)

More information: [Project Page](http://www.cs.umd.edu/~hui/code/hypercubemap)
