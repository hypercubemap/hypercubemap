% generating advertiser bids and budgets
for i = 1:9
    user = 1000*10^(ceil(i/2));
    if mod(i,2) == 0;        
        user = user*5;
    end

    % number of ads is determined by ad user ratio 
    ad = user/1000;
    
    for j = 1:5
        alpha = 1.2;

	% lognormal for bid
        bid = min(7, lognrnd(-1, 1, 1, ad));
        K = 1/alpha;
        sigma = mean(bid)*user*15/ad;
	% pareto for budget 
        budget = gprnd(K,sigma,sigma/K, 1, ad);

        Ads = [(0:ad-1); bid; budget];
        Ads = Ads';
        if mod(i,2) == 1;
            fid = strcat('Ads', int2str(10^(ceil(i/2))), 'k', int2str(j), '.csv');
        else
            fid = strcat('Ads', int2str(5*10^(ceil(i/2))), 'k', int2str(j), '.csv');
        end
        csvwrite(fid, Ads);
    end
end
