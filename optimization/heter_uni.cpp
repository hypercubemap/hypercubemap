// cpp implementation for fan-shape Lp wiht uniform node density

#include <ilcplex/ilocplex.h>
#include <ilconcert/ilocsvreader.h>
#include <vector>
#include <set>
#include <map>
#include <string>
#include <cmath>
#include <cstring>
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <sstream>
#include <algorithm>
#include <fenv.h>

using namespace std;

typedef IloArray<IloNumArray>    NumMatrix;
typedef IloArray<IloNumVarArray> NumVarMatrix;

// define data structure 

// advertiser
struct advertiser {
	IloInt id;
	IloNum bid;
	IloNum budget;	

	advertiser() {}

	advertiser(IloCsvLine& line) {
		id = line.getIntByPosition(0);
		bid = line.getFloatByPosition(1);
		budget = line.getFloatByPosition(2);		
	}

	advertiser(const advertiser& cp_ad) {
		id = cp_ad.id;
		bid = cp_ad.bid;
		budget = cp_ad.budget;
	}
};

// isolate cube
struct ic {
	IloInt spectrum;
	long long int id;
	IloNum start_theta;
	IloNum end_theta;
	IloNum start_r;
	IloNum end_r;
	IloNum start_uniform_r;
	IloNum end_uniform_r;
	IloInt start_degree;
	IloInt end_degree;

	ic() {}

	ic(IloCsvLine& line) {
		spectrum = line.getIntByPosition(0);
		id = atoll(line.getStringByPosition(1));
		start_theta = line.getFloatByPosition(2);
		end_theta = line.getFloatByPosition(3);
		start_r = line.getFloatByPosition(4);
		end_r = line.getFloatByPosition(5);
		start_uniform_r = line.getFloatByPosition(6);
		end_uniform_r = line.getFloatByPosition(7);
		start_degree = line.getIntByPosition(8);
		end_degree = line.getIntByPosition(9);
	}

	ic(const ic& cp_ic) {
		spectrum = cp_ic.spectrum;
		id = cp_ic.id;
		start_theta = cp_ic.start_theta;
		end_theta = cp_ic.end_theta;
		start_r = cp_ic.start_r;
		end_r = cp_ic.end_r;
		start_uniform_r = cp_ic.start_uniform_r;
		end_uniform_r = cp_ic.end_uniform_r;
		start_degree = cp_ic.start_degree;
		end_degree = cp_ic.end_degree;
	}
};

const char* get_filename(const char* data_dir, int r, const char* csv_file) {
	stringstream file_name;
	file_name << data_dir << "/" << r << "/" << csv_file;
	return file_name.str().c_str();
}

int main(int argc, char** argv) {
	if (argc < 3) {
		cout << "usage: cmd data_dir ad_file" << endl;
		return 0;
	}
	const char* data_dir = argv[1];
	const char* ad_csv_loc = argv[2];

	feenableexcept(FE_INVALID);

	int num_graphs = 25;
	double stop_condition = 1e-5;

	// 0. global constant
	IloNum pi = 3.14159265; 
	IloNum e = 2.71828; 
	IloNum w = 0.003;
	
	// a bitmap of targeting constraint
	map<long long int, set<IloInt> > oic_entries;
	// all advertisers
	vector<advertiser> Ads;

	IloNum sum_time = 0;
	try {
	    // Exit if no advertiser exist, or no graph left
	    for(int r = 1; r <= num_graphs; r++) {
			// cplex environment init object
			IloEnv env;

			if (oic_entries.size() < 1) {
		    		// 1. Read oic_ad.csv as global data
				char const delimiters[] = ";\n\r";   
				cout << "read: " << get_filename(data_dir, 1, "oic_ad.csv") << endl;
				IloCsvReader reader_oic(env, get_filename(data_dir, 1, "oic_ad.csv"), IloFalse, IloFalse, ",");
				IloCsvTableReader::LineIterator it_oic(reader_oic.getReaderForUniqueTableFile());
				int var_cnt = 0;
		  		for (; it_oic.ok(); ++it_oic) {
					IloCsvLine line = *it_oic;
					long long int oic = atoll(line.getStringByPosition(0));
					oic_entries[oic];
					char* ad_ids = line.getStringByPosition(1);
					char *ad_id = strtok(ad_ids, delimiters);
					while (0 != ad_id) {
						// insert value to hashmap value set
					 	oic_entries[oic].insert(atoi(ad_id));
					    ad_id = strtok(NULL, delimiters);
					}
					var_cnt += oic_entries[oic].size();
				}
		    		reader_oic.end();
				cout << "total num of vars: " << var_cnt << endl;
			}

			if (Ads.size() < 1) {
				// 1. ad data as global data, Read ads.csv
				cout << "read: " << ad_csv_loc << endl;
				IloCsvReader reader(env, ad_csv_loc);
				IloCsvTableReader::LineIterator it(reader.getReaderForUniqueTableFile());
		  		for (; it.ok(); ++it) {
					IloCsvLine line = *it;
		  			advertiser curr_ad(line);
					Ads.push_back(curr_ad);
				}
		    		reader.end();
			}
			IloInt num_ads = Ads.size();


			// loop var (util)
			IloInt i, ad;
			// init a cplex model object by env object
			IloModel model(env);
			// all isolate cubes
			vector<ic> Ics;

			// 1. load data for a unit graphs
			// 1.1. read meta file, Read constants
			IloNum a = IloInfinity;
			IloNum c = IloInfinity;

			cout << "read: " << get_filename(data_dir, r, "meta.txt") << endl;
			IloCsvReader reader_meta(env, get_filename(data_dir, r, "meta.txt"));
			IloCsvTableReader::LineIterator it_meta(reader_meta.getReaderForUniqueTableFile());
	      		for (; it_meta.ok(); ++it_meta) {
				IloCsvLine line = *it_meta;
				if(strcmp(line.getStringByPosition(0), "min_a") == 0) {
					a = line.getFloatByPosition(1);
				} else if (strcmp(line.getStringByPosition(0), "c") == 0) {
					c = line.getFloatByPosition(1);
				}
			}
	        	reader_meta.end();

	        	// 1.2. Read spectrum.csv and constant R
			IloNum R = -1;
			cout << "read: " << get_filename(data_dir, r, "spectrum.csv") << endl;
			IloCsvReader reader_ic(env, get_filename(data_dir, r, "spectrum.csv"));
			IloCsvTableReader::LineIterator it_ic(reader_ic.getReaderForUniqueTableFile());
	      		for (; it_ic.ok(); ++it_ic) {
				IloCsvLine line = *it_ic;
	      			ic curr_ic(line);
				Ics.push_back(curr_ic);
				R = max(R, curr_ic.end_r);
			}
	        	reader_ic.end();

			IloInt num_ics = Ics.size();

			// 2. build model and solve
			// 2.1 define decision variable 
			//-- to build the revert decisionv variable using sparse matrix	
			NumVarMatrix allocated(env, num_ics);
			//-- enumerate all the ics to add in the ads
			for (i = 0; i < num_ics; i++) {
				// sparse matrix, only use the ads associated with the oic
				allocated[i] = IloNumVarArray(env, oic_entries[Ics[i].id].size(), 0.0, IloInfinity, ILOFLOAT);
			}


			cout << "before targeting constraint" << endl;
			// 2.2. define the constraints
			// 2.2.1. targeting constraint 
			// no need anymore, because, the sparse matrix representation won't introduce redundant vars

			cout << "before budget constraint" << endl;			
			cout << "R: = " << R << endl;

			// 2.2.2. budget constraint 
			// - need to use ad_i for the index of a ic's associated ad 
			map<IloInt, IloExpr*> ad_budget; 
			for (i = 0; i < num_ics; i++) {
				set<IloInt>::iterator it = oic_entries[Ics[i].id].begin();
				for(int ad_i = 0; it != oic_entries[Ics[i].id].end(); it++, ad_i++) { 
					IloInt ad = *it;
					if (ad_budget.find(ad) == ad_budget.end()) {
						ad_budget[ad] = new IloExpr(env);
					}
					(*ad_budget[ad]) += Ads[ad].bid*a*(2.0*w*c*(sqrt((pow(e, R)-1)*pow(Ics[i].end_uniform_r,2.0)+pow(R,2.0))
						-sqrt((pow(e, R)-1)*pow(Ics[i].start_uniform_r,2.0)+pow(R,2.0)))
						+(pow(e,R)-1)*(pow(Ics[i].end_uniform_r,2.0)-pow(Ics[i].start_uniform_r,2.0))/R) 
						*allocated[i][ad_i]/2.0/pi/R;
				}
			}
			for (ad = 0; ad < num_ads; ad++) {
				if (ad_budget.find(ad) != ad_budget.end()) {
					model.add((*ad_budget[ad]) <= Ads[ad].budget);
	            			ad_budget[ad]->end();
					delete ad_budget[ad];
				}
			}
			ad_budget.clear();

			cout << "before range constraint" << endl;
			// 2.2.3. ic angular coordinate range constraint 
			for (i = 0; i < num_ics; i++) {
				IloExpr volume(env);
				set<IloInt>::iterator it = oic_entries[Ics[i].id].begin();
				for(int ad_i = 0; it != oic_entries[Ics[i].id].end(); it++, ad_i++) { 
					volume += allocated[i][ad_i];
				}
				model.add(volume <= Ics[i].end_theta - Ics[i].start_theta);
		        	volume.end();
			}

			cout << "before revenue" << endl;

			// define the objective function 
			IloExpr revenue(env);
			for (i = 0; i < num_ics; i++) {
				set<IloInt>::iterator it = oic_entries[Ics[i].id].begin();
				for(int ad_i = 0; it != oic_entries[Ics[i].id].end(); it++, ad_i++) { 
					IloInt ad = *it;
					revenue += Ads[ad].bid*a*(2.0*w*c*(sqrt((pow(e, R)-1)*pow(Ics[i].end_uniform_r,2.0)+pow(R,2.0))
						-sqrt((pow(e, R)-1)*pow(Ics[i].start_uniform_r,2.0)+pow(R,2.0)))
						+(pow(e,R)-1)*(pow(Ics[i].end_uniform_r,2.0)-pow(Ics[i].start_uniform_r,2.0))/R) 
						*allocated[i][ad_i]/2.0/pi/R;
				}
			}
			model.add(IloMaximize(env, revenue));
			revenue.end();


			cout << "before solving the model" << endl;

			// run model 
			IloCplex cplex(env);
			IloNum start_time = cplex.getTime();
			cplex.extract(model);
			//cplex.exportModel("heter_uni.lp");
			cout << "start to solve" << endl;
			cplex.solve();
			IloNum elapse_time = cplex.getTime() - start_time;
			sum_time += elapse_time;
			cout << "[Time]: " << elapse_time << endl;
			

			// 3. deduct budget
			IloNum residual_budget = 0;
			// comment out the decision variable value if needed 
			//-for (ad = 0; ad < num_ads; ad++) {
			//-	for (i = 0; i < num_ics; i++) {
			//-		Ads[ad].budget -= Ads[ad].bid*a*(2.0*w*c*(sqrt((pow(e, R)-1)*pow(Ics[i].end_uniform_r,2.0)+pow(R,2.0))
			//-		-sqrt((pow(e, R)-1)*pow(Ics[i].start_uniform_r,2.0)+pow(R,2.0)))
			//-		+(pow(e,R)-1)*(pow(Ics[i].end_uniform_r,2.0)-pow(Ics[i].start_uniform_r,2.0))/R) 
			//-		*cplex.getValue(allocated[ad][i])/2.0/pi/R;
			//-	}
			//-	residual_budget += Ads[ad].budget;
			//-}
			for (i = 0; i < num_ics; i++) {
				set<IloInt>::iterator it = oic_entries[Ics[i].id].begin();
				for(int ad_i = 0; it != oic_entries[Ics[i].id].end(); it++, ad_i++) { 
					IloInt ad = *it;
					Ads[ad].budget -= Ads[ad].bid*a*(2.0*w*c*(sqrt((pow(e, R)-1)*pow(Ics[i].end_uniform_r,2.0)+pow(R,2.0))
						-sqrt((pow(e, R)-1)*pow(Ics[i].start_uniform_r,2.0)+pow(R,2.0)))
						+(pow(e,R)-1)*(pow(Ics[i].end_uniform_r,2.0)-pow(Ics[i].start_uniform_r,2.0))/R) 
						*cplex.getValue(allocated[i][ad_i])/2.0/pi/R;
				}
			}
			for (ad = 0; ad < num_ads; ad++) {
				residual_budget += Ads[ad].budget;
			}

			// 4. print out current iteration
			// print out result 
			env.out() << "Round[" << r << "] status: \t" << cplex.getStatus() << endl;
			env.out() << "Round[" << r << "] revenue: \t" << cplex.getObjValue() << endl;
			env.out() << "Round[" << r << "] residual_budget \t" << residual_budget << endl;

	    		// Exit condition
	    		if (residual_budget < stop_condition) {
	    			break;
	    		}
	    		env.end();
	    	}
		cout << "[TotalTime]: " << sum_time << endl;
	}
	catch (IloException& e) {
		cerr << "ERROR: " << e.getMessage() << endl;
	}
	cout << "[TotalTime]: " << sum_time << endl;
   	return 0;
}
