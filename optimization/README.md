Optimal Allocation 
==========================

Introduction
-----------------------------------
This folder contains the CPLEX solvers for the original integer programming problem as well as the hyperbolic space region allocation problems.

* **Ad Allocation**: The integer program for the original ad allocation problem. The program read data, create objective and constraints, and solve the optimization problem. 

		heterIP.cpp

* **Region Allocation**: These programs are the CPLEX models for the region allocation problems. *heterfan.cpp* is for the exponential node distribution setting, while *heter\_uni.cpp* is for the uniform node distribution. These programs basically load data, create objective function and constraints, and solve the optimization problem.  

		heterfan.cpp
		heter_uni.cpp

Dependencies and Compilation
-----------------------------------
The solvers depend on CPLEX 12.6 (earlier versions may work).
Makefile is provided. To compile, simply run the make, it will compile the the three solvers. One may need to change the CPLEXDIR and CONCERTDIR path before compilation.

	make 
		
Run with an example
-----------------------------------
The command line arguments are:

	./some_solver data_dir ad_file	

data\_dir is the output of [the embedding program](https://bitbucket.org/hypercubemap/hypercubemap/src/423fdfb04e9c52883100f51034973d90d2cb9a18/embedding/?at=master). ad\_file is the output of [the ad generator](https://bitbucket.org/hypercubemap/hypercubemap/src/423fdfb04e9c52883100f51034973d90d2cb9a18/misc/?at=master). To test the programs using a simple dataset, the 100K embedded network and related ad data is provided.  

	unzip 100K_embed_data.zip

To run region allocations:

	./heterfan 100000 Ads100k1.csv
	./heter_uni 100000 Ads100k1.csv

To run the ad allocation IP:

	./heterIP 100000 Ads100k1.csv

All the data we used and experiments results are provided in our [Project Page](http://www.cs.umd.edu/~hui/code/hypercubemap). One can also provide her own network and advertiser ad, use our embedding program to embed the network then use the program here to solve the allocation problem.

