// cpp implementation for baseline IP

#include <ilcplex/ilocplex.h>
#include <ilconcert/ilocsvreader.h>
#include <vector>
#include <set>
#include <map>
#include <string>
#include <cmath>
#include <cstring>
#include <cstdio>
#include <cstdlib>
#include <iostream>

using namespace std;

ILOSTLBEGIN

typedef IloArray<IloNumArray>    NumMatrix;
typedef IloArray<IloIntVarArray> NumVarMatrix;

// define data structure 

// advertiser
struct advertiser {
	IloInt id;
	IloNum bid;
	IloNum budget;

	advertiser() {}

	advertiser(IloCsvLine& line) {
		id = line.getIntByPosition(0);
		bid = line.getFloatByPosition(1);
		budget = line.getFloatByPosition(2);
	}

	advertiser(const advertiser& cp_ad) {
		id = cp_ad.id;
		bid = cp_ad.bid;
		budget = cp_ad.budget;
	}
};

//user
struct user {
	/* data */
	IloInt id;
	long long int oic_id;
	IloInt impression;
	//IloInt degree;
	vector<IloInt> neighbor;

	user() {}

	user(const user& cp_user) {
		id = cp_user.id;
		oic_id = cp_user.oic_id;
		impression = cp_user.impression;
		//no degree information needed in this setting
		//degree = cp_user.degree;
		neighbor = cp_user.neighbor;
	}
};

const char* get_filename(const char* data_dir, int r, const char* csv_file) {
	stringstream file_name;
	file_name << data_dir << "/" << r << "/" << csv_file;
	return file_name.str().c_str();
}

void done(const char* filename) {
	cout << "[Done]" << filename << endl;
}

// a bitmap of targeting constraint
map<long long int, set<IloInt> > oic_entries;

// user id --> array index
map<int, int> user_map;

void free_memory() {
	oic_entries.clear();
	user_map.clear();
}

int main(int argc, char** argv) {
	if (argc < 3) {
		cout << "usage: cmd data_dir ad_file" << endl;
	}
	const char* data_dir = argv[1];
	const char* ad_csv_loc = argv[2];
	
	// cplex environment init object
	IloEnv env;
	try {
		// loop var (util)
		IloInt u, n, ad;
		// init a cplex model object by env object
		IloModel model(env);

		// improve from csv 
		vector<advertiser> Ads;
		//vector<ic> Ics;
		vector<user> Users;
		// ctr		
		IloNum w = 0.003;

		// 1. Read ads.csv
		IloCsvReader reader(env, ad_csv_loc);
		IloCsvTableReader::LineIterator it(reader.getReaderForUniqueTableFile());
      		for (; it.ok(); ++it) {
			IloCsvLine line = *it;
      			advertiser curr_ad(line);
			Ads.push_back(curr_ad);
		}
        	reader.end();

		done(ad_csv_loc);

		cout << "file: " << get_filename(data_dir, 1, "user_oic.csv") << endl;

		IloCsvReader reader_uoic(env, get_filename(data_dir, 1, "user_oic.csv"));
		IloCsvTableReader::LineIterator it_uoic(reader_uoic.getReaderForUniqueTableFile());
      		for (; it_uoic.ok(); ++it_uoic) {
			IloCsvLine line = *it_uoic;
      			user curr_user;
      			curr_user.id = line.getIntByPosition(0);
      			curr_user.oic_id = atoll(line.getStringByPosition(1));
			Users.push_back(curr_user);
			user_map[curr_user.id] = Users.size() - 1;
		}
        	reader_uoic.end();


		done(get_filename(data_dir, 1, "user_oic.csv"));

		cout << get_filename(data_dir, 1, "imp.csv") << endl;
		IloCsvReader reader_imp(env, get_filename(data_dir, 1, "imp.csv"));
		IloCsvTableReader::LineIterator it_imp(reader_imp.getReaderForUniqueTableFile());
      		for (; it_imp.ok(); ++it_imp) {
			IloCsvLine line = *it_imp;
      			int curr_user_id = line.getIntByPosition(0);
      			Users[user_map[curr_user_id]].impression = line.getIntByPosition(1);
		}
        	reader_imp.end();

		done(get_filename(data_dir, 1, "imp.csv"));

        	char const delimiters[] = ";\n\r";
        	IloCsvReader reader_adj(env, get_filename(data_dir, 1, "adj_graph.csv"), IloFalse, IloFalse, ",");
		IloCsvTableReader::LineIterator it_adj(reader_adj.getReaderForUniqueTableFile());
      		for (; it_adj.ok(); ++it_adj) {
			IloCsvLine line = *it_adj;
      			int curr_user_id = line.getIntByPosition(0);
			char* user_neighbors = line.getStringByPosition(1);
			char* user_neighbor = strtok(user_neighbors, delimiters);
      			while (0 != user_neighbor) {
				Users[user_map[curr_user_id]].neighbor.push_back(atoi(user_neighbor));
				user_neighbor = strtok(NULL, delimiters);
      			}
		}
        	reader_adj.end();

		done(get_filename(data_dir, 1, "adj_graph.csv"));        

        	// 3. Read oic_ad.csv
		IloCsvReader reader_oic(env, get_filename(data_dir, 1, "oic_ad.csv"), IloFalse, IloFalse, ",");
		IloCsvTableReader::LineIterator it_oic(reader_oic.getReaderForUniqueTableFile());
      		for (; it_oic.ok(); ++it_oic) {
			IloCsvLine line = *it_oic;
			long long int oic = atoll(line.getStringByPosition(0));
			// init hashmap key
			oic_entries[oic];
			char* ad_ids = line.getStringByPosition(1);
			char* ad_id = strtok(ad_ids, delimiters);
			while (0 != ad_id) {
				// insert value to hashmap value set
			 	oic_entries[oic].insert(atoi(ad_id));
			    ad_id = strtok(NULL, delimiters);
			}
		}
        	reader_oic.end();

		done(get_filename(data_dir, 1, "oic_ad.csv")); 

		// oic_entries: -> oic -> {ad}
		// curr_user.oic_id : user's oic
		IloInt num_ads = Ads.size();
		IloInt num_users = Users.size();

		// define decision variable 
		// sparse matrix representation
		NumVarMatrix allocated(env, num_users);

		// objective 
		IloExpr revenue(env);

		map<IloInt, IloExpr*> ad_budget; 
		// iterate over the user map, to construct the constraint
		map<int, int>::iterator user_it = user_map.begin();
		int user_i = 0; // hashmap key order
		for(; user_it != user_map.end(); user_it++, user_i++) {
			int user_id = user_it->first;
			int index = user_it->second;

			// decision variable
			// here is the declaration of the decision variable second dimension
			allocated[user_i] = IloIntVarArray(env, oic_entries[Users[index].oic_id].size(), 0,  100);

			// combine impression constraint in one loop
			IloExpr volume(env);

			IloNum pu = 0;
			for(int n = 0; n < Users[index].neighbor.size(); n++){
				pu += w*min(Users[index].impression, Users[Users[index].neighbor[n]].impression);
			}
			set<IloInt>::iterator it = oic_entries[Users[index].oic_id].begin();
			for(int ad_i = 0; it != oic_entries[Users[index].oic_id].end(); it++, ad_i++) { 
				IloInt ad = *it;
				if (ad_budget.find(ad) == ad_budget.end()) {
					ad_budget[ad] = new IloExpr(env);
				}
				// note user_i, and ad_i is used for the decision variable index
				(*ad_budget[ad]) += Ads[ad].bid * allocated[user_i][ad_i] * (1+pu);

				// objective function
				revenue += Ads[ad].bid * allocated[user_i][ad_i] * (1+pu);

				// combine impression constraint in one loop				
				volume += allocated[user_i][ad_i];
			}

			// combine impression constraint in one loop
			model.add(volume <= Users[index].impression);
        		volume.end();	
		}
		for (int ad = 0; ad < num_ads; ad++) {
			if (ad_budget.find(ad) != ad_budget.end()) {
				model.add((*ad_budget[ad]) <= Ads[ad].budget);
    				ad_budget[ad]->end();
				delete ad_budget[ad];
			} else {
				cout << "[warning] ad: " << ad << " no budget constraint! " << endl;
			}
		}
		ad_budget.clear();

		// objective
		model.add(IloMaximize(env, revenue));
		revenue.end();
	
		free_memory();
		Ads.clear();
		Users.clear();	

		// run model 
		IloCplex cplex(env);
		// set 1 to branch & cut, set 2 to dynamic search, set 0 by default is auto
		cplex.setParam(IloCplex::MIPSearch, 1);
		IloNum start_time = cplex.getTime();
		cplex.extract(model);
		//cplex.exportModel("heterIP.lp");
		cplex.solve();		
		IloNum elapse_time = cplex.getTime() - start_time;
		cout << "[Time]: " << elapse_time << endl;

		// print out result 
		env.out() << "Solution status: " << cplex.getStatus() << endl;
		env.out() << " - Solution: " << endl;
		env.out() << "   Revenue = " << cplex.getObjValue() << endl;
	}
   	catch (IloException& e) {
      	cerr << "ERROR: " << e.getMessage() << endl;
   	}
   	env.end();
   	return 0;
}
