// hyperbolic embedding algorithm implementation

#include <iostream>
#include <fstream>
#include <string>
#include <boost/unordered_map.hpp>
#include <boost/unordered_set.hpp>
#include <map>
#include <set>
#include <algorithm>
#include <vector>
#include <ctime>
#include <random>
#include <cmath>
#include <cstdlib>
#include <cstdio>
#include <cstring>

#include <gsl/gsl_sf_zeta.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_histogram.h>

#include <boost/functional/hash.hpp>

#define _USE_MATH_DEFINES

using namespace std;

typedef map<int, vector<int>, greater<int> > degree_levels;

#define LOGNORMAL "log-normal"
#define POISSON "poisson"

// advertisers
// fb: 1 billion users, 1 million advertisers, 
double USER_AD_RATIO = 0.001;
int AD_LOWER = 10;

// individual user groups
// in facebook case, 1 billion user, 0.5 million group
double USER_GROUP_RATIO = 0.0005;
int USER_GROUP_LOWER = 100;

int DEG_SPECTRUM = 10;

fstream out_c;
fstream out_a;
fstream out_meta;
fstream out_embd; 
fstream out_deg;
fstream out_imp;
fstream out_spectrum;
fstream out_graph;
fstream out_oic_ad;
fstream out_user_oic;


void build_node_deg_map(fstream& fs, boost::unordered_map<int, int>& node_degs, 
		boost::unordered_map<int, set<int> >& graph) {
	string line = "";
	getline(fs, line);
	int num_nodes = -1;
	while (fs.good()) {
		int from, to;
		if (line[0] != '#') {
			int s = line.find('\t');
		        from = stoi(line.substr(0, s));
			to = stoi(line.substr(s, line.length()));	
			if (node_degs.find(from) == node_degs.end())
				node_degs[from] = 1;
			else
				node_degs[from]++;
			if (node_degs.find(to) == node_degs.end())
				node_degs[to] = 1;
			else
				node_degs[to]++;
			graph[from].insert(to);
			graph[to].insert(from);
		} else {
			int s = line.find("# Nodes: ");
			if (s != string::npos) {
				size_t start = strlen("# Nodes: ");
				num_nodes = stoi(line.substr(start, line.length()));
			}
		}	
		getline(fs, line);
	}
	// in case, one node has no neighbor
	for (int i = 0; i < num_nodes; i++) node_degs[i];
}

void build_inv_deg_levels(boost::unordered_map<int, int>& node_degs, 
		map<int, vector<int>, greater<int> >& inv_deg_sets) {
	inv_deg_sets.clear();
	boost::unordered_map<int, int>::iterator it = node_degs.begin();
	// write degree csv 
	for(; it != node_degs.end(); it++) {
		out_deg << it->first << "," << it->second << endl;
	}
	it = node_degs.begin();
	// build the inverse deg set
	for(; it != node_degs.end(); it++) 
		inv_deg_sets[it->second].push_back(it->first);
}


struct embed_info {
	// Constant for degree distribution
	// c*e^(-r/2) = degree at r
	double c;
	// Constant for node density
	// a*e^r = node density at r
	double a;
	// the maximum a
	double min_a;
};

struct embed_coord {
	double r;
	double theta;
	double uniform_r;
};


// inner structure of spectrum info
struct ic_embed_coord {
	double start_theta;
	double end_theta;
};

// [begin_degree, end_degree)
struct spectrum_info
{
	boost::unordered_map<unsigned int, ic_embed_coord> ic_coords;
	int num_users;
	double begin_r;
	double end_r;
	double begin_uni_r;
	double end_uni_r;
	int begin_degree;
	int end_degree;
};


// build the degree spectrum 
// -- given all the node degrees
// -- given all the node's oics
// output
// -- the spectrums
// -- the inv_deg_sets: for backward compatibility
void build_deg_spectrums(boost::unordered_map<int, int>& node_degs, 
		boost::unordered_map<int, unsigned int>& user_2_oic,
		vector<spectrum_info>& spectrums, 
		map<int, vector<int>, greater<int> >& inv_deg_sets) {
	spectrums.clear();
	inv_deg_sets.clear();

	boost::unordered_map<int, int>::iterator it = node_degs.begin();
	// write degree csv 
	for(; it != node_degs.end(); it++) {
		out_deg << it->first << "," << it->second << endl;
	}

	it = node_degs.begin();
	// build the inverse deg set
	for(; it != node_degs.end(); it++) 
		inv_deg_sets[it->second].push_back(it->first);
	
	// build the spectrums, using the inverse deg set
	map<int, vector<int>, greater<int> >::iterator deg_iter = inv_deg_sets.begin();
	if (deg_iter == inv_deg_sets.end()) return;
	int curr_deg = deg_iter->first;
	int next_begin_deg = curr_deg - DEG_SPECTRUM;
	int curr_spectrum_users = 0;
	boost::unordered_map<unsigned int, boost::unordered_set<int> > curr_spectrum;
	do {
		// assign angular for previous spectrum, change to a new spectrum
		if (curr_deg <= next_begin_deg) {
			// to assign angular
			boost::unordered_map<unsigned int, boost::unordered_set<int> >::iterator spec_ic_iter = curr_spectrum.begin();
			spectrum_info curr_spectrum_info;
			// by enumerating over all ic in the spectrum
			curr_spectrum_info.begin_degree = next_begin_deg + DEG_SPECTRUM;
			curr_spectrum_info.end_degree = next_begin_deg;
			double curr_ang = 0;
			for(; spec_ic_iter != curr_spectrum.end(); spec_ic_iter++) {
				unsigned int oic_id = spec_ic_iter->first;
				ic_embed_coord oic_coord;
				oic_coord.start_theta = curr_ang;
				oic_coord.end_theta = curr_ang + spec_ic_iter->second.size()*1.0/curr_spectrum_users * M_PI * 2;
				curr_ang = oic_coord.end_theta;
				curr_spectrum_info.ic_coords[oic_id] = oic_coord;
			}

			// add to the spectrum
			spectrums.push_back(curr_spectrum_info);

			// change to new spectrum
			curr_spectrum.clear();
			curr_spectrum_users = 0;
			next_begin_deg = min(next_begin_deg, curr_deg) - DEG_SPECTRUM;
		}

		// accumulate the current spectrum
		// by adding the users under the current degree one by one
		vector<int>& curr_users = deg_iter->second;
		for(int j = 0; j < curr_users.size(); j++) {
			int user_id = curr_users[j];
			unsigned int oic_id = user_2_oic[user_id];
			curr_spectrum[oic_id].insert(user_id);
			curr_spectrum_users++;
		}
		// increase the deg iterator
		deg_iter++;
		// update the current pointed degree
		curr_deg = deg_iter->first;
	} while(deg_iter != inv_deg_sets.end());

	if (curr_spectrum.size() > 0) {
		// to assign angular
		boost::unordered_map<unsigned int, boost::unordered_set<int> >::iterator spec_ic_iter = curr_spectrum.begin();
		spectrum_info curr_spectrum_info;
		// by enumerating over all ic in the spectrum
		double curr_ang = 0;
		for(; spec_ic_iter != curr_spectrum.end(); spec_ic_iter++) {
			unsigned int oic_id = spec_ic_iter->first;
			ic_embed_coord oic_coord;
			oic_coord.start_theta = curr_ang;
			oic_coord.end_theta = curr_ang + spec_ic_iter->second.size()*1.0/curr_spectrum_users * M_PI * 2;
			curr_ang = oic_coord.end_theta;
			curr_spectrum_info.ic_coords[oic_id] = oic_coord;
		}
		curr_spectrum_info.begin_degree = next_begin_deg + DEG_SPECTRUM;
		curr_spectrum_info.end_degree = max(0, next_begin_deg);
		curr_spectrum_info.num_users = curr_spectrum_users;
		// add to the spectrum
		spectrums.push_back(curr_spectrum_info);
	}
}

void gen_impression(int num_nodes, boost::unordered_map<int, int>& node_imps, string distr) {
	cout << "num_nodes first time: " << num_nodes << endl;
	map<int, int> tr_num;
	if (distr.compare(POISSON) == 0) {
		default_random_engine generator(time(0));
		// mean is 3
		poisson_distribution<int> distr(10);	
		for (int i = 0; i < num_nodes; i++) {
			// at least 1, use rejection sampling
		 	int visit = distr(generator);
			while (visit == 0) 
		 		visit = distr(generator);
			node_imps[i] = visit;
			tr_num[visit]++;
		}	
		
	}
	for (map<int, int>::iterator it = tr_num.begin(); it != tr_num.end(); it++) {
		cout << it->first << " : " << it->second <<endl; 
	}
}

embed_info hetero_hyper_embed(map<int, embed_coord>& user_embedding, 
	boost::unordered_map<int, unsigned int>& user_2_oic,
	vector<spectrum_info>& spectrums,  int N, degree_levels& inv_deg_sets, double alpha=2.2) {
	user_embedding.clear();
	embed_info eb_info;
	default_random_engine generator(time(0));
	// the current user id sorted in the degree sequence
	double beta = 1.0/(alpha-1);
	int t = 0;
	double sum_c = 0;
	double sum_a = 0;
	double min_a = 0;
	// enumerate each spectrums
	for(int i = 0; i < spectrums.size(); i++) {
		double r_begin = N*log(N);
		double r_end = 0;
		double r_uni_begin = N*log(N);
		double r_uni_end = 0;
		for (int deg = spectrums[i].begin_degree; deg > spectrums[i].end_degree || (deg == 0 && spectrums[i].end_degree == 0); deg--) {
			vector<int>& node_set = inv_deg_sets[deg];
			for (int n = 0; n < node_set.size(); n++) {
				t++;
				embed_coord cord;
				// according to HyperMap.cpp
				cord.r = beta*2*log(t) + (1-beta)*2*log(N);
				// use the transformation to get the uniform distributed r
				cord.uniform_r = 2*log(N) * sqrt((pow(M_E, cord.r) - 1)/(pow(M_E, 2*log(N)) - 1));
				// note: c*e^(-r/2) = degree at r
				out_c << "c at r(" << cord.r << "): " << deg / pow(M_E, -cord.r/2.0) << endl;
				sum_c += deg / pow(M_E, -cord.r/2.0);	
				// note: a*e^r = node density at r
				out_a << "a at r(" << cord.r <<"): " << t/pow(M_E, cord.r) << endl;
				sum_a += t/pow(M_E, cord.r);
				min_a = t/pow(M_E, cord.r);
				// derive the spectrum r min and r max
				r_begin = min(r_begin, cord.r);
				r_end = max(r_end, cord.r);
				r_uni_begin = min(r_uni_begin, cord.uniform_r);
				r_uni_end = max(r_uni_end, cord.uniform_r);

				// assign theta according to the isolate cube
				ic_embed_coord curr_ic_theta = spectrums[i].ic_coords[user_2_oic[node_set[n]]];
				uniform_real_distribution<double> unif(curr_ic_theta.start_theta, curr_ic_theta.end_theta);
				cord.theta = unif(generator);
				user_embedding[node_set[n]] = cord;	
			}
		}
		spectrums[i].begin_r = r_begin;
		spectrums[i].end_r = r_end;
		spectrums[i].begin_uni_r = r_uni_begin;
		spectrums[i].end_uni_r = r_uni_end;
	}
	out_c << "avg c: " << sum_c / N << endl;
	out_a << "avg a: " << sum_a / N << endl;
	eb_info.c = sum_c/N;
	eb_info.a = sum_a/N;
	eb_info.min_a = min_a;
	return eb_info;
}

// write actor->oic, oic->user, graph
// write the graph, ad-user, ad-oic
void hetero_write_oic(boost::unordered_map<int, set<int> >& graph, 
	boost::unordered_map<unsigned int, boost::unordered_set<int> >& oic, 
	boost::unordered_map<unsigned int, boost::unordered_set<int> >& oic_2_ads, 
	boost::unordered_map<int, unsigned int>& user_2_oic) {

	out_graph << "Field|NAMES,id, neighbors" << endl;
	boost::unordered_map<int, set<int> >::iterator iter = graph.begin();	
	for (; iter != graph.end(); iter++) {
		out_graph << iter->first << ", ";
		int split_cnt = 0;
		set<int>::iterator nb_iter = iter->second.begin();
		for (; nb_iter != iter->second.end(); nb_iter++) {
			if (split_cnt++ > 200) {
				out_graph << endl << iter->first << ", ";
				split_cnt = 0;
			}
			out_graph << *nb_iter << ";";
		}
		out_graph << endl;
	}

	out_oic_ad << "Field|NAMES,oic_id, ad_id " << endl;
	boost::unordered_map<unsigned int, boost::unordered_set<int> >::iterator oic_iter = oic_2_ads.begin();
	for(; oic_iter != oic_2_ads.end(); oic_iter++) {
		out_oic_ad << oic_iter->first << ", ";
		int split_cnt = 0;
		boost::unordered_set<int>::iterator ad_iter = oic_iter->second.begin();
		for (; ad_iter != oic_iter->second.end(); ad_iter++) {
			if (split_cnt++ > 200) {
				out_oic_ad << endl << oic_iter->first << ", ";
				split_cnt = 0;
			}
			out_oic_ad << *ad_iter << ";";
		}
		out_oic_ad << endl;
	}

	out_user_oic << "Field|NAMES,user_id, oic_id " << endl;
	boost::unordered_map<int, unsigned int>::iterator user_oic_iter = user_2_oic.begin();
	for(; user_oic_iter != user_2_oic.end(); user_oic_iter++) {
		out_user_oic << user_oic_iter->first << ", " << user_oic_iter->second << endl;
	}
}

// write the uniform r
void hetero_write_embedding(map<int, embed_coord>& embedding, embed_info& einfo, 
	vector<spectrum_info>& spectrums) {
	map<int, embed_coord>::iterator itr = embedding.begin();
	out_embd << "Field|NAMES,node_id, r, uniform_r, theta" << endl;
	for (; itr != embedding.end(); itr++) {
		out_embd <<itr->first<<", " << itr->second.r << ", " << itr->second.uniform_r << ", "<< itr->second.theta << endl;
	}
	out_meta << "Field|NAMES,meta,value" << endl;
	out_meta << "a," << einfo.a << endl; 
	out_meta << "c," << einfo.c << endl; 
	out_meta << "min_a," << einfo.min_a << endl; 
	// write spectrums
	out_spectrum << "Field|NAMES,spectrum_id, oic_id, start_theta, end_theta, start_r, end_r, start_uniform_r, end_uniform_r, start_degree, end_degree" << endl;
	for (int i = 0; i < spectrums.size(); i++) {
		if (spectrums[i].num_users > 0) {
			boost::unordered_map<unsigned int, ic_embed_coord>::iterator iter = spectrums[i].ic_coords.begin();
			while (iter != spectrums[i].ic_coords.end()) {
				out_spectrum << i  << ", " << iter->first << ", " << iter->second.start_theta  << ", " << iter->second.end_theta << ", "
					<< spectrums[i].begin_r << ", " << spectrums[i].end_r << ", " 
					<< spectrums[i].begin_uni_r << ", " << spectrums[i].end_uni_r << ", " 
					<< spectrums[i].begin_degree << ", " << spectrums[i].end_degree << endl;
				iter++;
			}
		}
	}
}

void init(string dir, int uid_iter) {
	char cmd[80];
	sprintf(cmd, "mkdir -p %s/%d", dir.c_str(), uid_iter);
        system(cmd);
	char new_dir_bf[50];
	sprintf(new_dir_bf, "%s/%d", dir.c_str(), uid_iter); 
	string new_dir(new_dir_bf); 
	out_c.open(new_dir + "/c_distr.txt", fstream::out);	
	out_a.open(new_dir + "/a_distr.txt", fstream::out);	
	out_meta.open(new_dir + "/meta.txt", fstream::out); 
	out_embd.open(new_dir + "/embedding.csv", fstream::out); 
	out_deg.open(new_dir + "/deg.csv", fstream::out); 
	out_imp.open(new_dir + "/imp.csv", fstream::out); 
	out_spectrum.open(new_dir + "/spectrum.csv", fstream::out);
	out_graph.open(new_dir + "/adj_graph.csv", fstream::out);
	out_oic_ad.open(new_dir + "/oic_ad.csv", fstream::out);
	out_user_oic.open(new_dir + "/user_oic.csv", fstream::out);
}

void cleanup() {
	out_c.close();
	out_a.close();
	out_meta.close();
	out_embd.close();
	out_deg.close();
	out_imp.close();
	out_spectrum.close();
	out_graph.close();
	out_oic_ad.close();
	out_user_oic.close();
}

void next_unit_impression_graph(boost::unordered_map<int, int>& node_imps, 
		boost::unordered_map<int, set<int> >& graph, boost::unordered_map<int, int>& node_degs) {
	for(boost::unordered_map<int, int>::iterator it = node_imps.begin(); it != node_imps.end(); it++) {		
		out_imp << it->first << "," << it->second << endl;
		// if it is still an active node
		if (it->second > 0) { 
			//1. substract node_imps one by one
			it->second--;
			// no impression left
			if (it->second < 1) {
				//2. if equals to 0, remove adjacency list in graph
				//3. update the node_degs
				set<int>& neighbors = graph[it->first];		
				for(set<int>::iterator nit = neighbors.begin(); nit != neighbors.end(); nit++) {
					// remove it from its neighbors
					graph[*nit].erase(it->first);
					node_degs[*nit]--;
				}
				// remove it from the graph
				graph.erase(it->first);
				node_degs.erase(it->first);
			}
		}
	}
}	

// return the # of user groups
// with a lower bound
int get_num_user_group(int user_size) {
	return max(USER_GROUP_LOWER, (int)(user_size * USER_GROUP_RATIO));
}

// return the # of advertisers 
// with a lower bound
int get_num_ads(int user_size) {
	return max(AD_LOWER, (int)(user_size * USER_AD_RATIO));
}

// when one don't know the population
// assume the population is infinity
// then the generalized harmonic number
// can be approximated as zeta function
double get_pdf_zipf(int rank, double skew) {
	// use zeta function, one can approximate the zipf pmf
	return pow(rank, -1*skew)/gsl_sf_zeta(skew);
}

// when one knows the population
// we can calculate the generalized harmonic number
double get_pdf_zipf(int rank, int N, double skew) {
	double base = 0;
	for (int k = 1; k <= N; k++) base += pow(k, -1*skew);
	return pow(rank, -1*skew)/base;
}

// generate the multinomial distribution of the groups
// one user belongs to one group only
// NOTE: alpha of the dirichlet is unit vector <1.0 .. >
// in this case, multinomial distr probability is  
// uniform in the dirichlet simplex
// k - the number of groups
// one can disable the dirichlet and fix the categorical distr
void generate_user_groups(boost::unordered_map<int, int>& node_degs, boost::unordered_map<int, boost::unordered_set<int> >& groups, int k) {
	gsl_rng * r = gsl_rng_alloc(gsl_rng_mt19937);
	double alpha[k];
	for (int i = 0; i < k; i++) alpha[i] = 1.0;
	// generate the multinomial distribution for the groups
	// use this to associate users to groups
	double theta[k];
	gsl_ran_dirichlet(r, k, alpha, theta);

	// -- print the generated categorical distr
	//	for (int i = 0; i < k; i++) 
	//	  	cout << "group " << i << ": " << theta[i] << endl;

	// use the generated categorical distr to assign the user indices to groups
	unsigned int N = node_degs.size();
	unsigned int user_groups[N];
	gsl_ran_discrete_t* discrete_table = gsl_ran_discrete_preproc(k, theta);
	for (int i = 0; i < N; i++) user_groups[i] = gsl_ran_discrete(r, discrete_table);

	gsl_ran_discrete_free(discrete_table);
	gsl_rng_free(r);

	// assign the user to the user groups
	boost::unordered_map<int, int>::iterator iter = node_degs.begin();
	int cnt = 0;
	// the user index id can be different from 0 .. N-1
	while(iter != node_degs.end()) {
		// the group index
		groups[user_groups[cnt]].insert(iter->first);
		iter++;
		cnt++;
	}
}

// do a zipf for the ad popularity
// without do the shuffle, assign a zipf
void generate_advertiser_popularity(double* ad_popularity, int NA, double skew=1.2) {
	for (int rank = 1; rank <= NA; rank++) {
		ad_popularity[rank-1] = get_pdf_zipf(rank, NA, skew);
	}
	// -- print ad popularity
	//for (int i = 0; i < NA; i++) {
	//	cout << "advertiser " << i << ": " << ad_popularity[i] << endl;
	//}
}

// do a zipf for the user group popularity
// first it shuffles the user groups to get the ranks
// then use the shuffled order (rank) to assign the popularity 
void generate_user_group_popularity(double* group_popularity, int NG, double skew=1.1) {
	vector<int> temp_vector;
	// shuffle the user group id
	for(int i = 0; i < NG; i++) temp_vector.push_back(i);
		random_shuffle(temp_vector.begin(), temp_vector.end());
	// assign the zipf distribution according to the shuffled order
	for (int rank = 1; rank <= NG; rank++) {
		group_popularity[temp_vector[rank-1]] = get_pdf_zipf(rank, NG, skew);
	}
	// -- print group popularity
	//for (int i = 0; i < NG; i++) {
	//	cout << "group " << i << ": " << group_popularity[i] << endl;
	//}
}

// utility function to print the historgram of a sequence
void print_historgram(int bin, int start, int end, unsigned int* seq, int seq_size) {
	gsl_histogram * h = gsl_histogram_alloc(bin);
    gsl_histogram_set_ranges_uniform (h, start, end);
    for (int i = 0; i < seq_size; i++) gsl_histogram_increment(h, seq[i]);
    gsl_histogram_fprintf (stdout, h, "%g", ": %g");
    gsl_histogram_free(h);
}

// generate two degree sequences using the two zipf distribution
// then permutate the two event list, then we get the bipartite graph
// how to set the total # of edges?
void bi_prefer_attach(double* ad_pop, int NA, double* group_pop, int NG, boost::unordered_map<int, set<int> >& group_ads) { 
	gsl_rng * r = gsl_rng_alloc(gsl_rng_mt19937);
	// ASSUMPTION: the edges equals to five times of user groups
	int num_edges = NG*5;
	// generate for advertiser side
	gsl_ran_discrete_t* discrete_table_ad = gsl_ran_discrete_preproc(NA, ad_pop);
	unsigned int ad_side[num_edges];
	boost::unordered_set<unsigned int> ads;
	for(int i = 0; i < NA; i++) ads.insert(i);
	for (int i = 0; i < num_edges; i++) {
		ad_side[i] = gsl_ran_discrete(r, discrete_table_ad);
		ads.erase(ad_side[i]);
	}
	//cout << "ad side edges historgram: " << endl;
	//print_historgram(NA, 0, NA, ad_side, num_edges);
		
	// generate for group side	
	gsl_ran_discrete_t* discrete_table_group = gsl_ran_discrete_preproc(NG, group_pop);
	unsigned int group_side[num_edges];
	boost::unordered_set<unsigned int> groups;
	for (int i = 0; i < NG; i++) groups.insert(i);
	for (int i = 0; i < num_edges; i++) {
		group_side[i] = gsl_ran_discrete(r, discrete_table_group);
		groups.erase(group_side[i]);
	}

	//cout << "user side edges historgram: " << endl;
	//print_historgram(NG, 0, NG, group_side, num_edges);

	//cout << "remaining ads: " << ads.size() << endl;
	//cout << "remaining groups: " << groups.size() << endl;

	// adjust to make sure there's no empty user group, or empty ad
	// fix both sides to make sure no empty node left
	int additional_size = ads.size() + groups.size();
	unsigned int additional_ad_side[additional_size];
	// for the rest of remaining group, each one add an ad
	for (int i = 0; i < groups.size(); i++) additional_ad_side[i] = gsl_ran_discrete(r, discrete_table_ad);
	// for remaining ads, append to the tail
	if (ads.size() > 0) {
		boost::unordered_set<unsigned int>::iterator iter = ads.begin();
		for (int i = groups.size(); i < additional_size; i++, iter++) {
			additional_ad_side[i] = *iter;
		}
	}

	unsigned int additional_group_side[additional_size];
	// for remaining groups, append to the tail
	if (groups.size() > 0) {
		boost::unordered_set<unsigned int>::iterator iter = groups.begin();
		for (int i = 0; i < groups.size(); i++, iter++) {
			additional_group_side[i] = *iter;
			//cout << "additional group: " << additional_group_side[i] << endl;
		}
	}
	// for the rest of remaining ads, each one add a group
	for (int i = groups.size(); i < additional_size; i++) additional_group_side[i] = gsl_ran_discrete(r, discrete_table_group);

	gsl_ran_discrete_free(discrete_table_ad);
	gsl_ran_discrete_free(discrete_table_group);
	gsl_rng_free(r);
	
	// combine the group_side, and additional group side, ad_side, and additional ad side
	// build the unordered_map<int, set<int> >& group_ads
	for (int i = 0; i < num_edges; i++) group_ads[group_side[i]].insert(ad_side[i]);
	for (int i = 0; i < additional_size; i++) group_ads[additional_group_side[i]].insert(additional_ad_side[i]);
}

// use boost hash combine to generate hash key for the set of ads
// then use the ads key to rehash the set of groups
// by definition, this is the optimal ic
void build_optimal_isolate_cube(boost::unordered_map<int, set<int> >& group_2_ads, boost::unordered_map<unsigned int, boost::unordered_set<int> >& oic, boost::unordered_map<unsigned int, boost::unordered_set<int> >& oic_2_ads) {
	boost::unordered_map<int, set<int> >::iterator iter = group_2_ads.begin();
	for(; iter!= group_2_ads.end(); iter++) {
		int group_id = iter->first;
		set<int>& ads = iter->second;
		unsigned int hash_key = boost::hash_range(ads.begin(), ads.end());
		oic[hash_key].insert(group_id);
		if (oic_2_ads[hash_key].size() < 1) oic_2_ads[hash_key].insert(ads.begin(), ads.end());
	}
	// full memory, it can be found in the oic_2_ads 
	group_2_ads.clear();
	// -- print total oic
	//iter = oic.begin();
	//cout << "total oic: " << oic.size() << endl;
	//int sum = 0;
	//for(; iter != oic.end(); iter++) {
	//	cout << iter->first << " : " << iter->second.size() << endl;
	//	sum += iter->second.size();
	//}
	//cout << sum << endl;
}

// build user to oic reverse map
// input  - groups, the group -> users map
// 		  - oic, the oic_id -> groups map
// output - user_2_oic, the user_id -> oic_id map
//		  - oic_2_user, the oic_id -> users map
void build_user_oic_rev_map(boost::unordered_map<int, boost::unordered_set<int> >& groups, 
	boost::unordered_map<unsigned int, boost::unordered_set<int> >& oic,
	boost::unordered_map<int, unsigned int>& user_2_oic,
	boost::unordered_map<unsigned int, boost::unordered_set<int> >& oic_2_user) {
	// enumerate over all oic
	boost::unordered_map<unsigned int, boost::unordered_set<int> >::iterator iter = oic.begin();
	for(; iter != oic.end(); iter++) {
		// the curr oic
		unsigned int curr_oic = iter->first;
		// enumerate all it's associated groups
		boost::unordered_set<int>& oic_groups = iter->second;
		boost::unordered_set<int>::iterator iter2 = oic_groups.begin();
		for(; iter2 != oic_groups.end(); iter2++) {
			// enumerate all a group's users
			boost::unordered_set<int>& users = groups[*iter2];
			boost::unordered_set<int>::iterator user_iter = users.begin();
			for(; user_iter != users.end(); user_iter++) {
				// build the user -> oic reverse map
				user_2_oic[*user_iter] = curr_oic;
				// build the oic -> {user} map
				oic_2_user[curr_oic].insert(*user_iter);
			}
		}
	}
}

int main(int argc, char** argv) {
	if (argc < 3) {
		cout << "usage: cmd network_filename outdir (spectrum_width)" << endl;	
		return 0;
	}
	if (argc > 3) {
		DEG_SPECTRUM = atoi(argv[3]);	
	}
	//load file, and build node degree, and the graph
	fstream fs;
	fs.open(argv[1], fstream::in);	
	// node and their degrees
	boost::unordered_map<int, int> node_degs;
	// the adjacency graph
	boost::unordered_map<int, set<int> > graph;
	build_node_deg_map(fs, node_degs, graph);
	fs.close();

	// generate user groups
	int num_groups = get_num_user_group(node_degs.size());
	cout << "user groups: " << num_groups << endl;
	// generate the categorical distribution for category
	// associate node to its group
	boost::unordered_map<int, boost::unordered_set<int> > groups;
	generate_user_groups(node_degs, groups, num_groups);
	// generate group popularity
	// do a permutation, and assign zipf with s = 1.1
	double group_popularity[groups.size()];
	generate_user_group_popularity(group_popularity, groups.size());

	// generate the advertisers
	int num_ads = get_num_ads(node_degs.size());
	cout << "ad groups: " << num_ads << endl;
	double ad_popularity[num_ads];
	generate_advertiser_popularity(ad_popularity, num_ads);

	// generate the ad-user_group bipartite preferential attachment graph
	boost::unordered_map<int, set<int> > group_2_ads;
	bi_prefer_attach(ad_popularity, num_ads, group_popularity, num_groups, group_2_ads);

	// do the optimal isolate cubes, id -> {ads}
	boost::unordered_map<unsigned int, boost::unordered_set<int> > oic;
	boost::unordered_map<unsigned int, boost::unordered_set<int> > oic_2_ads;
	build_optimal_isolate_cube(group_2_ads, oic, oic_2_ads);
	cout << "optimal isolate cubes: " << oic.size() << endl;

	// generate the user -> oic, oic -> user maps
	boost::unordered_map<int, unsigned int> user_2_oic;
	boost::unordered_map<unsigned int, boost::unordered_set<int> > oic_2_user;
	build_user_oic_rev_map(groups, oic, user_2_oic, oic_2_user);

	// embed the cubes together with the spectrum and uniform distribution

	// # of nodes
	int num_nodes = node_degs.size();
	// node impression
	boost::unordered_map<int, int> node_imps;	
	gen_impression(num_nodes, node_imps, POISSON);
	int uid_iter = 1;
	// unit decomposition
	while(num_nodes > 0) {
		cout << "num_nodes: " << num_nodes << endl;
		//init out dir
		init(argv[2], uid_iter++);
		// degree and node with that degree, in desc degree order 
		degree_levels inv_deg_sets; 
		vector<spectrum_info> spectrums;
		build_deg_spectrums(node_degs, user_2_oic, spectrums, inv_deg_sets);

		//embedding coord
		map<int, embed_coord> embedding;
		// start embedding
		embed_info einfo = hetero_hyper_embed(embedding, user_2_oic, spectrums, num_nodes, inv_deg_sets);

		// write embedding to file
		hetero_write_embedding(embedding, einfo, spectrums);

		// write actor->oic, oic->user, graph
		// write the graph, ad-user, ad-oic
		hetero_write_oic(graph, oic, oic_2_ads, user_2_oic);

		// use graph, node_imps, rebuilt node_degs, update num_nodes	
		next_unit_impression_graph(node_imps, graph, node_degs);
		num_nodes = node_degs.size();
		// close fs
    		cleanup();	
	} 
	return 0;
}

