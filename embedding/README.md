hb_emb_hetero.cpp
==========================

Introduction
-----------------------------------
The program is the implementation of HyperCubeMap hyperbolic embedding algorithm for complex networks. For the ease of generation, before the embedding, it does the bidding bipartite graph generation between a given network and a set of ads. 

The major steps in the program is the following:

* **Social Influence**: Determine the influence function for each node in the given network.

		build_node_deg_map

* **Impression**: Generate the user impression vector using a Poisson distribution.

		gen_impression

* **User Groups**: Generate user groups, and user-group belonging using a multinomial distribution, which is generated randomly using a dirichlet prior. Assign user-group's bidding popularity using a zipf distribution.

		generate_user_groups
		generate_user_group_popularity

* **Advertisers**: Generate advertisers' popularity, meaning different advertisers have different # of target groups. The more popular advertiser will bid more target user groups.  

		generate_advertiser_popularity

* **Bidding**: Use bipartite preferential attachment to generate bidding (edges between user group and advertisers) according to their popularity distributions.

		bi_prefer_attach

* **Optimal Isolate Cubes**: Using the bidding information, cluster the user groups into optimal isolate cubes. 

		build_optimal_isolate_cubes

* **HyperCubeMap Embedding**: Using the generated user information (social influence and impression), the bidding information (user-ad bipartite graph), as well as the degree spectrum width parameter, embed the SNS to hyperbolic space cubes (Optimal Isolate Cubes on Degree Spectrum).

		build_deg_spectrums
		hetero_hyper_embed
		

* **Unit Impression Decomposition**: Generate the next unit impression graph after an embedding. It cleans the network too by removing nodes with no residual impression.  

		next_unit_impression_graph

* **Embedding Output**: Finally, it outputs the files to be used in the optimization programs. The files are csv files with a header line required by CPLEX csv reader. The files are: embedding.csv (coordinates), meta.txt (constants), spectrum.csv (annulus with start, end radius), adj\_graph.csv (# of neighbors of each node, used in HembIP), oic\_ad.csv (clustered bidding between ads and optimal isolate cubes, used in HembExp and HembUni), user\_oic.csv (user - optimal isolate cubes, containment relationships for tracing back to users after region allocation)

		hetero_write_embedding
		hetero_write_oic


Dependencies and Compilation 
-----------------------------------
The implementation is compiled with g++ 4.6.3.
It depends on: gsl, gslcblas, and need c++0x. 

to compile:

	g++ -o hb_emb  hb_emb_hetero.cpp -lgsl -std=c++0x -lgslcblas


Run with an example
-----------------------------------
The command line arguments are:

	./hb_emb network_file embedding_directory degree_spectrum_width[optional]	

The data directory has a generated network file with 5M nodes. To run the program with the data: 

	unzip data/plgraph_5M.zip
	./hb_emb plgraph_5000000_2.2.txt out_dir 10

The output first prints the impression distribution, then it does the bipartite preferential attachment. After that, the embeddings and unit graphs are generated accordingly. 

Embedding Data used in our experiment
-----------------------------------
All generated bidding and embedding graphs are listed in the data set section in our site: [Project Page](http://www.cs.umd.edu/~hui/code/hypercubemap). The files are large, one needs to download zip parts, and uncompress them to use. Or one can use different network files using SNAP's graph format, and only embed her own graph. We provided the power law graphs used in our experiment in our project page too.
